core: 8.x
api: 2
defaults:
  projects:
    subdir: contrib
projects:
  drupal:
    type: core
    download:
      type: git
      url: 'https://github.com/drupal/core.git'
      tag: 8.5.1
  admin_toolbar:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/admin_toolbar'
      tag: 1.x-23.0
  adminimal_admin_toolbar:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/adminimal_admin_toolbar'
      tag: 1.x-5.0
  coffee:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/coffee'
      tag: 1.x-0.0-beta2
  config_filter:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/config_filter'
      tag: 1.x-1.0
  config_ignore:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/config_ignore'
      tag: 2.x-1.0
  config_split:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/config_split'
      tag: 1.x-3.0
  contribute:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/contribute'
      tag: 1.x-0.0-beta7
  ctools:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/ctools'
      tag: 3.x-0.0
  entity_reference_revisions:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/entity_reference_revisions'
      tag: 1.x-4.0
  hn:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/hn'
      branch: 1.x
      revision: fce27ece0d19e79b922c0d5bd86f041204a932b9
  metatag:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/metatag'
      tag: 1.x-5.0
  paragraphs:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/paragraphs'
      tag: 1.x-2.0
  pathauto:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/pathauto'
      tag: 1.x-1.0
  redirect:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/redirect'
      tag: 1.x-1.0
  simple_sitemap:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/simple_sitemap'
      tag: 2.x-11.0
  token:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/token'
      tag: 1.x-1.0
  webform:
    type: module
    download:
      type: git
      url: 'https://git.drupal.org/project/webform'
      tag: 5.x-0.0-rc3
  adminimal_theme:
    type: theme
    download:
      type: git
      url: 'https://git.drupal.org/project/adminimal_theme'
      tag: 1.x-3.0
libraries:
  drupal/console:
    type: library
    download:
      type: git
      url: 'https://github.com/hechoendrupal/drupal-console.git'
      branch: 1.8.0
      revision: 368bbfa44dc6b957eb4db01977f7c39e83032d18
  drush/drush:
    type: library
    download:
      type: git
      url: 'https://github.com/drush-ops/drush.git'
      branch: 8.1.16
      revision: bbaff2dc725a5f3eb22006c5de3dc92a2de54b08

